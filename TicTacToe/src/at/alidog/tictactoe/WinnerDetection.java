package at.alidog.tictactoe;

import java.util.ArrayList;

public class WinnerDetection {

	public String checkWin(ArrayList<Field> myFields, int size) {

		String line = "";
		int counter = 0;
		Field currentField = new Field(null);
		String winner = "noWin";
		Boolean unfinished = false;

		// check winner row
		line = "";
		for (Field field : myFields) {
			counter++;
			line += field.getType();
			if (size == 3) {
				if (line.equals("xxx")) {
					winner = "x";
				}

				if (line.equals("ooo")) {
					winner = "o";
				}
			} else if (size == 4) {
				if (line.equals("xxxx")) {
					winner = "x";
				}

				if (line.equals("oooo")) {
					winner = "o";
				}
			} else {
				if (line.equals("xxxxx")) {
					winner = "x";
				}
				if (line.equals("ooooo")) {
					winner = "o";
				}
				if (counter == size) {
					counter = 0;
					line = "";
				}
			}
		}
		// check winner column
		line = "";
		if (size == 3) {
			// 3x3 board
			for (int i = 0; i < size; i++) {
				currentField = myFields.get(i);
				line += currentField.getType();
				currentField = myFields.get(i + 3);
				line += currentField.getType();
				currentField = myFields.get(i + 6);
				line += currentField.getType();

				if (line.equals("xxx")) {
					winner = "x";
				}

				if (line.equals("ooo")) {
					winner = "o";
				}

				line = "";

			}
		} else if (size == 4) {
			// 4x4 Board
			for (int i = 0; i < size; i++) {
				currentField = myFields.get(i);
				line += currentField.getType();
				currentField = myFields.get(i + 4);
				line += currentField.getType();
				currentField = myFields.get(i + 8);
				line += currentField.getType();
				currentField = myFields.get(i + 12);
				line += currentField.getType();

				if (line.equals("xxxx")) {
					winner = "x";
				}

				if (line.equals("oooo")) {
					winner = "o";
				}

				line = "";

			}
		} else {
			// 5x5 Board
			for (int i = 0; i < size; i++) {
				currentField = myFields.get(i);
				line += currentField.getType();
				currentField = myFields.get(i + 5);
				line += currentField.getType();
				currentField = myFields.get(i + 10);
				line += currentField.getType();
				currentField = myFields.get(i + 15);
				line += currentField.getType();
				currentField = myFields.get(i + 20);
				line += currentField.getType();

				if (line.equals("xxxxx")) {
					winner = "x";
				}

				if (line.equals("ooooo")) {
					winner = "o";
				}

				line = "";
			}
		}
		
		// check winner diagonal
		line = "";
		if (size == 3) {
			// 3x3 board
			currentField = myFields.get(0);
			line += currentField.getType();
			currentField = myFields.get(4);
			line += currentField.getType();
			currentField = myFields.get(8);
			line += currentField.getType();

			if (line.equals("xxx")) {
				winner = "x";
			}

			if (line.equals("ooo")) {

				winner = "o";
			}

			line = "";

			currentField = myFields.get(2);
			line += currentField.getType();
			currentField = myFields.get(4);
			line += currentField.getType();
			currentField = myFields.get(6);
			line += currentField.getType();

			if (line.equals("xxx")) {
				winner = "x";
			}

			if (line.equals("ooo")) {
				winner = "o";
			}

			line = "";

		} else if (size == 4) {
			// 4x4 Board
			currentField = myFields.get(0);
			line += currentField.getType();
			currentField = myFields.get(5);
			line += currentField.getType();
			currentField = myFields.get(10);
			line += currentField.getType();
			currentField = myFields.get(15);
			line += currentField.getType();

			if (line.equals("xxxx")) {
				winner = "x";
			}

			if (line.equals("oooo")) {

				winner = "o";
			}

			line = "";

			currentField = myFields.get(3);
			line += currentField.getType();
			currentField = myFields.get(6);
			line += currentField.getType();
			currentField = myFields.get(9);
			line += currentField.getType();
			currentField = myFields.get(12);
			line += currentField.getType();

			if (line.equals("xxxx")) {

				winner = "x";
			}

			if (line.equals("oooo")) {

				winner = "o";
			}

			line = "";

		} else {
			// 5x5 Board
			currentField = myFields.get(0);
			line += currentField.getType();
			currentField = myFields.get(6);
			line += currentField.getType();
			currentField = myFields.get(12);
			line += currentField.getType();
			currentField = myFields.get(18);
			line += currentField.getType();
			currentField = myFields.get(24);
			line += currentField.getType();

			if (line.equals("xxxxx")) {
				winner = "x";
			}

			if (line.equals("ooo0o")) {
				winner = "o";
			}

			line = "";

			currentField = myFields.get(4);
			line += currentField.getType();
			currentField = myFields.get(8);
			line += currentField.getType();
			currentField = myFields.get(12);
			line += currentField.getType();
			currentField = myFields.get(16);
			line += currentField.getType();
			currentField = myFields.get(20);
			line += currentField.getType();

			if (line.equals("xxxxx")) {
				winner = "x";
			}

			if (line.equals("ooo0o")) {
				winner = "o";
			}

			line = "";
		}
		if (!winner.equals("x") && !winner.equals("o")) {
			// check for a draw
			for (Field field : myFields) {
				if (field.getType().equals("x") || field.getType().equals("o")) {

				} else {
					unfinished = true;
				}

			}

			if (!unfinished) {
				return "draw";
			}
		}
		return winner;

	}

}
