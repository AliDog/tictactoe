package at.alidog.tictactoe;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		ArrayList<Field> myFields = new ArrayList<Field>();
		Field currentField = new Field(null);
		String number = null;
		Printer printer = new Printer();
		boolean isFinished = false;
		WinnerDetection winnerDetection = new WinnerDetection();
		String winner = null;
		int counter = 0;
		int input = 0;
		int turn = 1;
		boolean valid = false;
		boolean draw = false;
		int size = 0;
		Scanner sc = new Scanner(System.in);
		
		//Ask player for size
		while (!valid) {
			System.out.println("Please enter the size (3-5)");
			size = sc.nextInt();
			if (size < 3 || size > 5) {
				System.out.println("Invalid Input! Please try again.");
			} else {
				valid = true;
			}
		}
		
		//generate fields
			for (int i = 0; i < size*size; i++) {
				myFields.add(new Field(Integer.toString(counter)));
				counter++;
			}
		while (!isFinished) {
			
			//print field
			printer.Print(myFields, size);
			
			//check for win or draw
			if (turn > size) {
				winner = winnerDetection.checkWin(myFields, size);
				if (winner.equals("x") || winner.equals("o")) {
					isFinished = true;
					break;
				} else if (winner.equals("draw")) {
					isFinished = true;
					draw = true;
					break;
				}
			}

			//Player x
			System.out.print("Player x:");
			input = sc.nextInt();
			currentField = myFields.get(input);

			number = currentField.getType();
			if (currentField.type == "x" || currentField.type == "o") {
				System.out.print("Invalid!\n");
			} else {
				currentField.type = "x";
				myFields.set(Integer.parseInt((number)), currentField);
			}
			
			//print field
			printer.Print(myFields, size);

			//check for win or draw
			if (turn >= size) {
				winner = winnerDetection.checkWin(myFields, size);
				if (winner.equals("x") || winner.equals("o")) {
					isFinished = true;
					break;
				} else if (winner.equals("draw")) {
					isFinished = true;
					draw = true;
					break;
				}
			}

			//Player o
			System.out.print("Player o:");
			input = sc.nextInt();
			currentField = myFields.get(input);

			number = currentField.getType();
			if (currentField.type == "x" || currentField.type == "o") {
				System.out.print("Invalid!\n");
			} else {
				currentField.type = "o";
				myFields.set(Integer.parseInt((number)), currentField);
			}

			turn++;

		}
		//announce draw or winner
		if (draw) {
			System.out.println("It's a Draw!");
		} else {
			System.out.println("Player " + winner + " wins!");
		}

	}

}