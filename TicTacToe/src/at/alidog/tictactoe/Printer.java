package at.alidog.tictactoe;

import java.util.ArrayList;

public class Printer {

	public void Print(ArrayList<Field> myFields, int size) {

		int counter = 0;
		for (Field field : myFields) {
			try {
				if (Integer.parseInt((field.getType())) < 10) {
					System.out.print(" | ");
				} else {
					System.out.print(" |");
				}
			} catch (Exception e) {
				System.out.print(" | ");
			}

			System.out.print(field.getType());
			counter++;
			if (counter == size) {
				System.out.print(" |\n\n");
				counter = 0;
			}
		}
	}

}
