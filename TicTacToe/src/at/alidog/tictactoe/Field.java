package at.alidog.tictactoe;

public class Field {

	String type;

	public Field( String type) {
		super();
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
